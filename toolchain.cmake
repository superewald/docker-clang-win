# Cross toolchain configuration for using clang-cl.

set(CMAKE_SYSTEM_NAME Windows)
set(CMAKE_SYSTEM_VERSION 10.0)
if("$ENV{CLANG_WIN_ARCH}" STREQUAL "x86")
    set(CMAKE_SYSTEM_PROCESSOR X86)
elseif("$ENV{CLANG_WIN_ARCH}" STREQUAL "x86_64")
    set(CMAKE_SYSTEM_PROCESSOR AMD64)
endif()

set(CMAKE_C_COMPILER "/usr/bin/clang-cl")
set(CMAKE_CXX_COMPILER "/usr/bin/clang-cl")
set(CMAKE_LINKER "/usr/bin/lld-link")

set(MSVC_BASE "/xwin/crt")
set(MSVC_INCLUDE "${MSVC_BASE}/include")
set(MSVC_LIB "${MSVC_BASE}/lib")

set(WINSDK_BASE "/xwin/sdk")
set(WINSDK_VER "10.0.22621")
set(WINSDK_INCLUDE "${WINSDK_BASE}/Include/${WINSDK_VER}")
set(WINSDK_LIB "${WINSDK_BASE}/Lib/${WINSDK_VER}")

# set 32 bit flags if x86
if("${CMAKE_SYSTEM_PROCESSOR}" STREQUAL "X86")
    set(CMAKE_C_FLAGS "-m32")
    set(CMAKE_CXX_FLAGS "-m32")
endif()

# append crt and sdk includes to standard include directories
foreach(LANG C CXX RC)
    list(APPEND CMAKE_${LANG}_STANDARD_INCLUDE_DIRECTORIES "${MSVC_INCLUDE}/")
    list(APPEND CMAKE_${LANG}_STANDARD_INCLUDE_DIRECTORIES "${WINSDK_INCLUDE}/ucrt")
    list(APPEND CMAKE_${LANG}_STANDARD_INCLUDE_DIRECTORIES "${WINSDK_INCLUDE}/shared")
    list(APPEND CMAKE_${LANG}_STANDARD_INCLUDE_DIRECTORIES "${WINSDK_INCLUDE}/um")
    list(APPEND CMAKE_${LANG}_STANDARD_INCLUDE_DIRECTORIES "${WINSDK_INCLUDE}/cppwinrt")
    list(APPEND CMAKE_${LANG}_STANDARD_INCLUDE_DIRECTORIES "${WINSDK_INCLUDE}/winrt")
endforeach()

# link crt/sdk libraries
link_directories("${MSVC_LIB}/$ENV{CLANG_WIN_ARCH}")
link_directories("${WINSDK_LIB}/ucrt/$ENV{CLANG_WIN_ARCH}")
link_directories("${WINSDK_LIB}/um/$ENV{CLANG_WIN_ARCH}")

if("${CMAKE_SYSTEM_PROCESSOR}" STREQUAL "X86")
    link_libraries("${MSVC_LIB}/x86/clang_rt.builtins-i386.lib" "${MSVC_LIB}/x86/legacy_stdio_definitions.lib")
endif()

# set linker machine flag
add_link_options(/manifest:no)
set(CMAKE_LINKER_FLAGS "")
if("${CMAKE_SYSTEM_PROCESSOR}" STREQUAL "X86")
    set(CMAKE_LINKER_FLAGS "${CMAKE_LINKER_FLAGS} /machine:x86")
else()
    set(CMAKE_LINKER_FLAGS "${CMAKE_LINKER_FLAGS} /machine:x64")
endif()

set(CMAKE_SHARED_LINKER_FLAGS "${CMAKE_LINKER_FLAGS}")
set(CMAKE_STATIC_LINKER_FLAGS "${CMAKE_LINKER_FLAGS}")
set(CMAKE_EXE_LINKER_FLAGS "${CMAKE_LINKER_FLAGS}")

# setup compiler options
add_compile_options(/EHsc -Qunused-arguments -w -Wno-invalid-token-paste)
add_compile_definitions(_NO_CRT_STDIO_INLINE)

set(CMAKE_INSTALL_SYSTEM_RUNTIME_LIBS_NO_WARNINGS ON)