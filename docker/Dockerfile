FROM alpine:3.19

ARG CLANG_WIN_ARCH

# install required libs
RUN set -eux; \
    apk add --no-cache gpg curl git make cmake; \
    apk add --no-cache clang17 llvm17 lld tar;

# install xwin and download windows crt and sdk to /xwin
RUN set -eux; \
    xwin_version="0.5.0"; \
    xwin_prefix="xwin-$xwin_version-x86_64-unknown-linux-musl"; \
    # Install xwin to cargo/bin via github release. Note you could also just use `cargo install xwin`.
    curl --fail -L https://github.com/Jake-Shadle/xwin/releases/download/0.5.0/xwin-0.5.0-x86_64-unknown-linux-musl.tar.gz | tar -xzv -C /usr/local/bin --strip-components=1 $xwin_prefix/xwin; \
    # Splat the CRT and SDK files to /xwin/crt and /xwin/sdk respectively
    xwin --accept-license --arch $CLANG_WIN_ARCH splat --include-debug-libs --output /xwin; \
    # Remove unneeded files to reduce image size
    rm -rf .xwin-cache /usr/local/bin/xwin;

COPY toolchain.cmake /xwin/toolchain.cmake

ENV CC="clang-cl" \
    CXX="clang-cl" \
    CL_FLAGS="-Wno-unused-command-line-argument -fuse-ld=lld-link /xwin/crt/include /xwin/sdk/include/ucrt /xwin/sdk/include/um /imsvc/xwin/sdk/include/shared" \
    CLANG_WIN_ARCH="$CLANG_WIN_ARCH" \
    CMAKE_TOOLCHAIN_FILE="/xwin/toolchain.cmake"